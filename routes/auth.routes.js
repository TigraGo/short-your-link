const {Router} = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {check, validationResult} = require('express-validator');
const config = require('config');
const User = require('../models/User');
const router = Router();

router.post('/register',
          [
            check('email', 'Incorrect e-mail').isEmail(), check('password', 'Incorrect password: min length for password is 6 symbold').isLength({min: 6})
          ], async (request, response) => {
  try {
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
      return response.status(400).json({
        errors: errors.array(),
        message: 'Incorrect data for registration'
      })
    }

    const {email, password} = request.body;
    const candidate = await User.findOne({email}) // key-value
    if (candidate) {
      return response.status(400).json({ message: 'User is already exists'});
    }

    const hashedPassword = await bcrypt.hash(password, 12);
    const user = new User({email, password: hashedPassword});
    
    await user.save();
    return response.status(201).json({ message: 'Successfully created user!'});
  } catch (e) {
    console.log(e)
    response.status(500).json({ message: 'Something went wrong, try again.' });
  }
})

router.post('/login',
          [
            check('email', 'Enter the correct e-mail').normalizeEmail().isEmail(), check('password', 'Incorrect password').exists()
          ], async (request, response) => {
  try {
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
      return response.status(400).json({
        errors: errors.array(),
        message: 'Incorrect data for login'
      })
    }

    const {email, password} = request.body;
    const user = await User.findOne({email});

    if (!user) {
      return response.status(400).json({ message: 'The user is not found' });
    }

    const isMatch = await bcrypt.compare(password, user.password)

    if (!isMatch) {
      return response.status(400).json({ Message: 'Incorrect e-mail or password, try again' });
    }

    const token = jwt.sign({userId: user.id, mail: user.email}, config.get('JWT_SECRET'), {expiresIn: '1h'});

    response.json({ token, userId: user.id}); // by default is 200

  } catch (e) {
    response.status(500).json({ message: 'Something went wrong, try again.' });
  }
})

module.exports = router;