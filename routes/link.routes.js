const {Router} = require('express')
const Link = require('../models/Link')
const config = require('config')
const shortid = require('shortid')
const router = Router()
const auth = require('../middleware/auth.middleware')

router.post('/generate', auth, async (request, response) => {
  try {
    const BASE_URL = config.get('BASE_URL')
    const {from} = request.body

    const code = shortid.generate()

    const existing = await Link.findOne({ from })

    if (existing) return response.json({link: existing})

    const to = BASE_URL + '/t/' + code
    const link = new Link({
      from, to, code, owner: request.user.userId
    })

    await link.save()
    response.status(201).json({link})
  } catch (e) {
    response.status(500).json({ message: 'Something went wrong, try again.' });
  }
})

router.get('/', auth, async (request, response) => {
  try {
    const links = await Link.find({ owner: request.user.userId })
    response.json(links)
  } catch (e) {
    response.status(500).json({ message: 'Something went wrong, try again.' });
  }
})

router.get('/:id', auth, async (request, response) => {
  try {
    const link = await Link.findById(request.params.id)
    response.json(link)
  } catch (e) {
    response.status(500).json({ message: 'Something went wrong, try again.' });
  }
})

module.exports = router