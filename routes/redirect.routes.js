const {Router} = require('express');
const Link = require('../models/Link')
const router = Router();

router.get('/:code', async (request, response) => {
  try {
    const link = await Link.findOne({code: request.params.code})
    if (link) {
      link.clickCount++
      await link.save()
      await response.redirect(link.from)
    }

    response.status(404).json({ message: 'Link not found' })
  } catch (e) {
    response.status(500).json({ message: 'Something went wrong, try again.' });
  }
})

module.exports = router