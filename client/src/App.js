import React from 'react';
import { useRoutes } from './Routes';
import {BrowserRouter as Router} from 'react-router-dom'
import { useAuth } from './hooks/auth.hook';
import 'antd/dist/antd.css';
import { AuthContext } from './context/AuthContext';
import { Navbar } from './components/Navbar/Navbar';
import { Loader } from './components/Loader/Loader';

function App() {
  const {token, login, logout, userId, ready} = useAuth()
  const isAuthenticated = !!token
  const routes = useRoutes(isAuthenticated)
  
  if (!ready) {
    return <Loader />
  }
  return (
    <AuthContext.Provider value={{token, login, logout, userId, isAuthenticated}}>
      <Router>
        {isAuthenticated ? <Navbar /> : null}
        {routes}
      </Router>
    </AuthContext.Provider>
  );
}

export default App;
