import React from 'react'
import { List } from 'antd'
import { Link } from 'react-router-dom'

export const LinksList = ({links}) => {
  if (!links.length) {
    return <div style={{textAlign: 'center'}}>No links yet</div>
  }
  return (
    <List bordered>
      {links.map((link, index) => (
        <List.Item key={index}>
          <div># {index + 1}</div>
          <div>Original: {link.from}</div>
          <div>Shortened: <a href={link.to} rel="noopener noreferrer" target="_blank">{link.to}</a></div>
          <div>Date of creation: {new Date(link.date).toLocaleDateString()}</div>
          <Link to={`detail/${link._id}`}>More</Link>
        </List.Item>
      ))}
    </List>
  )
}