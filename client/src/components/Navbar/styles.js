import styled from '@emotion/styled'

export const NavbarWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`