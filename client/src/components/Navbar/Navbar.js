import React, { useContext } from 'react'
import { NavLink, useHistory } from 'react-router-dom'
import { AuthContext } from '../../context/AuthContext'
import { Button } from 'antd'
import { NavbarWrapper } from './styles'

export const Navbar = () => {
  const auth = useContext(AuthContext)
  const history = useHistory()

  const logoutHandler = event => {
    event.preventDefault()
    auth.logout()
    history.push('/')
  }

  return (
    <NavbarWrapper>
      <Button type="link">
        <NavLink to="/links">Links</NavLink>
      </Button>
      <Button type="link">
        <NavLink to="/create">Create Link</NavLink>
      </Button>
      <Button type="link">
        <a href="/" onClick={logoutHandler}>Logout</a>
      </Button>
    </NavbarWrapper>
  )
  
}