import React from 'react'
import { Card } from 'antd'

export const LinkCard = ({link}) => {
  return (
    <>
      <Card title="Your Link">
        <a href={link.to} target="_blank" rel="noopener noreferrer">{link.to}</a>
      </Card>
      <Card title="From">
        <a href={link.from} target="_blank" rel="noopener noreferrer">{link.from}</a>
      </Card>
      <Card title="Count of clicked">
        {link.clickCount}
      </Card>
      <Card title="Created date">
        {new Date(link.date).toLocaleDateString()}
      </Card>
    </>
  )
}