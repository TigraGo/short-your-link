import React, {useState, useEffect, useContext} from 'react'
import { Row, Col, Input, Button } from 'antd';
import { useHttp } from '../hooks/http.hook';
import { useMessage } from '../hooks/message.hook';
import { AuthContext } from '../context/AuthContext';


export const AuthorizationPage = () => {
  const auth = useContext(AuthContext)
  const message = useMessage()
  const {loading, error, request, clearError} = useHttp()
  const [form, setForm] = useState({
    email: '', password: ''
  })

  useEffect(() => {
    message(error)
    clearError()
  }, [error, message, clearError])

  const changeHandler = event => {
    setForm({...form, [event.target.name]: event.target.value})
  }

  const registerHandler = async () => {
    try {
      const data = await request('/api/auth/register', 'POST', {...form})
      message(data.message)
    } catch (e) {}
  }

  const loginHandler = async () => {
    try {
      const data = await request('/api/auth/login', 'POST', {...form})
      auth.login(data.token, data.userId)
    } catch (e) {}
  }

  return (
    <Row style={{margin: '20px auto', justifyContent: 'center'}}>
      <Col>
        <h2 style={{textAlign: 'center'}}>Short your link! But authorize first!</h2>
        <Input placeholder="Enter your e-mail" style={{marginBottom: '20px'}} name="email" value={form.email} onChange={changeHandler}  />
        <Input placeholder="Enter your password" type="password" name="password" value={form.password} onChange={changeHandler} />

        <Button style={{margin: '20px 20px 20px 0'}} onClick={loginHandler} disabled={loading}>Login</Button>
        <Button type="primary" onClick={registerHandler} disabled={loading}>
          Register
        </Button>
      </Col>
    </Row>
  )
  
}