import React, { useContext, useState, useCallback, useEffect } from 'react'
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';
import { LinksList } from '../components/LinksList/LinksList';

export const LinksPage = () => {
  const [links, setLinks] = useState([])
  const {token} = useContext(AuthContext)
  const {request, loading} = useHttp()
  const fetchLinks = useCallback( async () => {
    try {
      const fetchedLinks = await request('/api/link/', 'GET', null, {
        Authorization: `Bearer ${token}`
      })
      setLinks(fetchedLinks)
    } catch (e) {}
  }, [token, request])

  useEffect(() => {
    fetchLinks()
  }, [fetchLinks])

  return (
    <>
      {!loading && links && <LinksList links={links} />}
    </>
  )
}