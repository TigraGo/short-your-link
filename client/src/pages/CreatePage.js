import React, {useState, useContext} from 'react'
import {useHistory} from 'react-router-dom'
import { Row, Col, Input } from 'antd';
import { useHttp } from '../hooks/http.hook'
import { AuthContext } from '../context/AuthContext';

export const CreatePage = () => {
  const auth = useContext(AuthContext)
  const history = useHistory()
  const {request} = useHttp()
  const [link, setLink] = useState('')

  const pressHandler = async event => {
    if (event.key === 'Enter') {
      try {
        const data = await request('/api/link/generate', 'POST', {from: link}, {
          Authorization: `Bearer ${auth.token}`
        })
        history.push(`/detail/${data.link._id}`)
      } catch (e) {

      }
    }
  }
  return (
    <Row style={{margin: '20px auto', justifyContent: 'center'}}>
      <Col>
        <h2 style={{textAlign: 'center'}}>Short your link!</h2>
        <Input
          placeholder="Enter your link" 
          style={{marginBottom: '20px'}} 
          name="link" 
          onChange={(e) => setLink(e.target.value)}  
          onKeyPress={pressHandler}
        />
{/* 
        <Button style={{margin: '20px 20px 20px 0'}} onClick={loginHandler} disabled={loading}>Login</Button>
        <Button type="primary" onClick={registerHandler}>
          Register
        </Button> */}
      </Col>
    </Row>
  )
}